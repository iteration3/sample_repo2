# Context diagram

```puml
@startdot
digraph contextDiagram {
    node[shape="record"]
    subgraph externalEntities {
        entity1[label="Customer"];
        entity2[label="Manager"];
    }
    subgraph cluster_System {
        label="System environment";
        process1[label="System" shape="ellipse"];
        graph[style=dotted]
    }
    entity1->process1[label="Request for services"];
    process1->entity2[label="Reports"];
}
@enddot
```

## Data flow diagram

```puml
@startdot
digraph dfd {
    node[shape=record]
    store1 [label="<f0> 1|<f1> Data store"];
    entity1 [label="User" shape=box];
    process1 [label="Process" shape="ellipse"];
    store1:f1 -> process1[label="Username, password"];
    entity1-> process1[label="Authenticate"];
}
@enddot
```

<!--Generated UML diagram-->

---
<img src="http://www.plantuml.com/plantuml/svg/NL4zRuCm4Dtv5PRt3ErQ4KLgEtP9HuGg1xycbenDxuweACf_Zo6ceTnY-UvzdLtiM0o9ZL9OToJJdLGJW-1Ntfn9zrPT2fKgH8kAJwP3gGbD9AlhQS3zuKvB550m_Zs84mVEnB4mjeQdofi3VAbVUvROWdJzie8ypv0F4ypnWRZ-jsfyKW1zVGxfQ1T6cPtx26T7CRH9Mp-SEeeDcF_ItC5QvUNWlUiOosoJQSKoU9JfeGIsNgJAosssQ-4zVdgmgEz8Ya5dbwQpx8pTR5UBxz55aWbtBNO8TloP6m00">
<!--Generated UML diagram-->

---
<img src="http://www.plantuml.com/plantuml/svg/RP3D3e9038Jl-nGDPqtaga0myG6yU28S2bk4P6KtsnerwhlBZu8nzZZpjJEfIbdGYxQYT7Fqw6hGbOQxWcvQgodZ6Xt5dahhTJx8BDPJ29d1WamSHDKgWV0HLM42EnGS_I3V332rqiXjfWzCFe3nQ66lRyfvMnBpZEr7uKC6P4pZUBewLbXN8IoJQVaxeiKJBS0XywNh_LFcpyxsB7NlbYX3o5EbrEh-BIy0">