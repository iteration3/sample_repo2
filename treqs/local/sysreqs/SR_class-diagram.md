# PlantUML code within markdown file

The following content is a sample class diagram

```puml
@startuml Class diagram for Meeting Scheduler
class Customer {
    Name
    Email
    Address
}
class MeetingInitiator {

}
class Participant{
}
note right of Participant
/'[requirement id=REQ0014 quality=QR0002 test=TC0001]'/

 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/qualityreqs/QR_qualityreqs.md QR0002]]
 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/tests/TC_system.py TC0001]]
end note
class Meeting{
    location
    startTime
    endTime
}
class Room{
    roomNumber
}
User <|--Participant
User <|--MeetingInitiator
Meeting - "1..1" Room
Participant - " *..1" Meeting
MeetingInitiator - "1..*" Participant
@enduml
```

## Another embedded PlantUML code

```puml
@startuml

Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
note right of Alice
/'[requirement id=REQ0012 test=TC0005]'/


 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/tests/TC_system.py TC0005]]
end note
@enduml
```

## Yet, another PlantUML code

```puml
@startuml
actor Foo1
boundary Foo2
control Foo3
entity Foo4
database Foo5
collections Foo6
Foo1 -> Foo2 : To boundary
Foo1 -> Foo3 : To control
Foo1 -> Foo4 : To entity
Foo1 -> Foo5 : To database
Foo1 -> Foo6 : To collections
note left of Foo1
/'[requirement id=REQ0013 quality=QR0002 test=TC0003]'/

 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/qualityreqs/QR_qualityreqs.md QR0002]]
 [[https://gitlab.com/treqs-chalmers/sample_repo1/blob/dev/treqs/local/tests/TC_manual.md TC0003]]
end note
@enduml
```

<!--Generated UML diagram-->

---
<img src="http://www.plantuml.com/plantuml/svg/hL9DRy8m3BtdLrXSa91esccdQKXC5OSTXaR7JWYXj3KGAMbAuav2ZF--D2q8jkjoUlvuTkndDdKaB3LQGQg4Sr1AiRD2mzPOU4CaMUtWezXZsIYqh0WvQUF8QBHmOk3VN6WCOAQ5L06zbAL5vzYvP_ILNYj9Kf1fcTVOk-yl2rcBYaxUMHb2i7At9p3Rso3ZmvN5GoCjQgm8P3d9PeiuJXxXq0WbwJXPP74SFm2XeyaozJXP3pbZi5hjYMhtnFbEaX9vL1ZDoHTpus8lb1_5SITqhN1ZiJO9pvN9UObVNH9NfX2Azrs2Pv5jRin8bz3rNg__grixXEFBTEEEZb17zH7waTOCgnBQFTrljrEZPPCqLJ22k4lPw-Df0LysdncZEvBrQDxet4jyPf_EI_lyFHxVBl_g_2qbkrp960P952M3K9RTSDi8Z4AepsL_xg4ZZmPtWa_zX_rbiXy0">
<!--Generated UML diagram-->

---
<img src="http://www.plantuml.com/plantuml/svg/XP1DQm9138RlyojoypHcBNYHMhJI7w1uMqHcPrDtOBuwoHRyzvtjUglGMo1FdlSbQiTYYep1AxNtpXBeDtXFtGRsemmKnLaZBaKuqTT8BAhkG5Vc5tv2SKwHwOyifieLCC-bC_ogzJ_qGrxN1CNT1e7qEJTHk6XBjRb2ePw0wxUdZsFJh5v0QiBsV6YQPdrPe58AsdOGoRn1l3dnfblQ553gEMix61-eCB89sTErK4uhx7pgiAVl6KAVhF4uUHdFXolVMIWiynqUAHT5iOUffThLQNhk3m00">
<!--Generated UML diagram-->

---
<img src="http://www.plantuml.com/plantuml/svg/hPB1YeCm48RlUOgvzTH6Qzj3mQLGTk-ADn69chP2ujXaNDYttyJeuWFiRVxvX__B964tIyBGf1KJBQ61ByIODJWDdJ0_NXrPYmCPL5uaJ0xKqsoSM2T8DC9AhyvkJ2dPKe-3zOqByr6m_vX3u0e5mfgxjP9WBP2jSmfE86wDSp1M_DQwh6b_Ps43aWGb7mJu2ElnNMdaU-gDr2uS-YxDFxCeYXDuJq8vM9hbKHGTWQIbjBYxEgbsd34eonVHQA-SFtjIeZcqg3cvCBjlNq9fQIotGezAraQE6FD6OSCx-Hs6kC9MABvGvawMrnjvq1q4TbNz5yqlONbnhxKO7CaZbfKgzveT-FjXDrVvJ_0B">